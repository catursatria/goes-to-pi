<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\User;

class AuthController extends Controller
{
    public function login()
    {
        return view('auths.login');
    }

    public function postlogin(Request $request)
    {
        if(Auth::attempt($request->only('email', 'password'))){
            $user = User::where([
                'email'  => $request->email,
            ])->first();
         

            if ($user->role == 'admin') {
                return redirect('/dashboard');
            } else {
                return redirect()->route('profiluser',['id' => $user->id]);
            }
        }
        return redirect('/login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
